core = 7.x
api = 2

; uw_studentbudget_calculator
projects[uw_studentbudget_calculator][type] = "module"
projects[uw_studentbudget_calculator][download][type] = "git"
projects[uw_studentbudget_calculator][download][url] = "https://git.uwaterloo.ca/wcms/uw_studentbudget_calculator.git"
projects[uw_studentbudget_calculator][download][tag] = "7.x-3.2"
projects[uw_studentbudget_calculator][subdir] = ""

